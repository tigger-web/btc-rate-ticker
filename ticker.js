/* ==========================
   BTC rate ticker javascript
   ========================== */

   
/* ---------
   Constants
   --------- */

// some calculation constants
const SECONDS_PER_MINUTE = 60
const MILLISECONDS_PER_SECOND = 1000

// settings
const REFRESH_SECONDS_API = 5 * SECONDS_PER_MINUTE;   // each 5min
const REFRESH_COUNTDOWN_INTERVAL = 250                // countdown refresh: 0.25sec
const MONEY_DIGITS = 2                                // display price with 2 digits (euro cents)
const KRAKEN_FETCH_URL = "https://api.kraken.com/0/public/Ticker?pair=XBTEUR"
const NOT_AVAILABLE_INFO = "n/a"

/* ----------------
   Global variables
   ---------------- */

// objects in page
let startButton
let stopButton
let rateSpan
let refreshInfoSpan

// other variables
let nextRefreshDate = null

/* -----------
   Boot loader
   ----------- */

// wait for document loaded before init components
document.addEventListener('DOMContentLoaded', function() { initComponents() })

/* ---------
   Functions
   --------- */

// Debug method will simply write string param to console
function dbg(info) {
    console.log(info)
}

// Fetch current rate from KRAKEN and update the ticker
// On error stop refreshing
function fetchRate() {
    fetch(KRAKEN_FETCH_URL)
        .then(function(response) {
            return response.json()
         })
        .then(function(data) {
            let rate = data.result.XXBTZEUR.c[0]
            console.log("rate fetched: " + rate)
            updateTicker(rate)

         })
        .catch(function(err) {
          console.log("Error while fetching data from " + KRAKEN_FETCH_URL)
          console.log(err)
          stopRefresh()
      })
}

// Update the refresh info with seconds left for next rate fetching
function updateRefreshInfo(seconds) {
    refreshInfoSpan.textContent = seconds
}

// Update the rate itself in display.
// Note param is a string contains a number with a lot of digits
function updateTicker(rate) {
    let content = isNaN(rate) ? rate : parseFloat(rate).toFixed(MONEY_DIGITS)
    rateSpan.textContent = content
}

// Store timestamp for next refresh to global variable: NOW + REFRESH_SECONDS_API
function updateNextRefreshDate() {
    nextRefreshDate = new Date()
    nextRefreshDate.setTime(nextRefreshDate.getTime() + REFRESH_SECONDS_API * MILLISECONDS_PER_SECOND)
}
  
// Interval method which calls itself as long as a nextRefreshDate is set
function updateRefreshInterval() {
    if (nextRefreshDate != null) {
        let currentDate = new Date()
        let difference = Math.round((nextRefreshDate.getTime() - currentDate.getTime()) / MILLISECONDS_PER_SECOND)
        if (difference <= 0) {
            fetchRate()
            updateNextRefreshDate()
            updateRefreshInfo(0)
        } else {
            updateRefreshInfo(difference)
        }
        setTimeout(updateRefreshInterval, REFRESH_COUNTDOWN_INTERVAL)
    }
}

// start button callback
function startRefresh() {
    startButton.disabled = true
    stopButton.disabled = false
    fetchRate()
    updateNextRefreshDate()
    updateRefreshInterval()
    dbg("rate refresh started")
}

// stop button callback
function stopRefresh() {
    startButton.disabled = false
    stopButton.disabled = true
    nextRefreshDate = null         // Note: this will prevent next internal timeout in updateRefreshInterval() 
    updateTicker(NOT_AVAILABLE_INFO)
    updateRefreshInfo(NOT_AVAILABLE_INFO)
    dbg("rate refreh stopped")
}

// init components (called after page is loaded)
function initComponents() {
    startButton = document.getElementById("btn_start")
    stopButton = document.getElementById("btn_stop")
    rateSpan = document.getElementById("rate-span")
    refreshInfoSpan = document.getElementById("refresh-info-span")
    
    updateTicker(NOT_AVAILABLE_INFO)
    updateRefreshInfo(NOT_AVAILABLE_INFO)
    startButton.onclick = function() { startRefresh() }
    stopButton.onclick = function() { stopRefresh() }
    stopButton.disabled = true
    
    dbg("components initalized")
}
