# BTC rate ticker

A simple javascript based ticker to display the current rate BTC => EUR

## Icon

FavIcon is using this symbol:

https://www.flaticon.com/free-icon/bitcoin-symbol-inside-circulating-arrows_38291

Thanks for the great free license, flaticon team!

## Kraken API

### About the API and their usage

* URL: https://api.kraken.com/0/public/Ticker?pair=XBTEUR
* API method: GET
* Parameter: XBTEUR (also BTCEUR) for BTC => EUR (XBT is the old symbol used by Kraken so it is part of response)
* Access: free but with rate limiter (not more than 1/second, so with 1/min or 1/15min there should not be a problem)
* CORS allowed

**Result:**

JSON structure as explained here: https://docs.kraken.com/rest/#operation/getTickerInformation

* pair is the ticker information, here "XXBTZEUR" for BTC => EUR
* "c->0 for "Last trade closed" [<today>, <last 24 hours>]

### Example

**Raw result:**

```json
{
  "error": [],
  "result": {
    "XXBTZEUR": {
      "a": ["52381.20000","2","2.000"],
      "b": ["52381.10000","1","1.000"],
      "c": ["52383.80000","0.00037616"],
      "v": ["149.48035515","2685.28430612"],
      "p": ["52506.07108","53242.46204"],
      "t": [4246,40337],
      "l": ["51885.00000","51632.10000"],
      "h": ["52885.50000","54757.20000"],
      "o": "52133.60000"
    }
  }
}
```

**Current price:**

`result -> XXBTZEUR -> c[0]` = 1BTC = 52383.80 EUR

## Implementation

Click "Start" will fetch curren rate from KRAKEN and also start an internal interval to
refresh the rate each 5mins.

Click "Stop" will stop  the internal interval.

The solution is using pure javascript only and base libraries:

* Date: fetch current date and deal with time differences (in milliseconds)
* Math: for rounding
* Fetch: fetching external JSON data
* Timeout: use to repeat method updateRefreshInterval() as long as nextRefreshDate is set